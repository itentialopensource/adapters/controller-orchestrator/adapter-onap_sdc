/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-onap_sdc',
      type: 'OnapSdc',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const OnapSdc = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Onap_sdc Adapter Test', () => {
  describe('OnapSdc Class Tests', () => {
    const a = new OnapSdc(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const distributionCatalogServletServiceName = 'fakedata';
    const distributionCatalogServletServiceVersion = 'fakedata';
    const distributionCatalogServletArtifactName = 'fakedata';

    describe('#downloadServiceArtifact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadServiceArtifact(distributionCatalogServletServiceName, distributionCatalogServletServiceVersion, distributionCatalogServletArtifactName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DistributionCatalogServlet', 'downloadServiceArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const distributionCatalogServletResourceInstanceName = 'fakedata';

    describe('#downloadResourceInstanceArtifactByName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadResourceInstanceArtifactByName(distributionCatalogServletServiceName, distributionCatalogServletServiceVersion, distributionCatalogServletResourceInstanceName, distributionCatalogServletArtifactName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DistributionCatalogServlet', 'downloadResourceInstanceArtifactByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const distributionCatalogServletResourceName = 'fakedata';
    const distributionCatalogServletResourceVersion = 'fakedata';

    describe('#downloadResourceArtifact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadResourceArtifact(distributionCatalogServletServiceName, distributionCatalogServletServiceVersion, distributionCatalogServletResourceName, distributionCatalogServletResourceVersion, distributionCatalogServletArtifactName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DistributionCatalogServlet', 'downloadResourceArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const distributionServletRegisterForDistributionBodyParam = {
      apiPublicKey: 'string',
      distrEnvName: 'string',
      isConsumerToSdcDistrStatusTopic: false,
      distEnvEndPoints: [
        'string'
      ]
    };

    describe('#registerForDistribution - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerForDistribution(distributionServletRegisterForDistributionBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.distrNotificationTopicName);
                assert.equal('string', data.response.distrStatusTopicName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DistributionServlet', 'registerForDistribution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const distributionServletUnRegisterForDistributionBodyParam = {
      apiPublicKey: 'string',
      distrEnvName: 'string',
      isConsumerToSdcDistrStatusTopic: true,
      distEnvEndPoints: [
        'string'
      ]
    };

    describe('#unRegisterForDistribution - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unRegisterForDistribution(distributionServletUnRegisterForDistributionBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.distrNotificationTopicName);
                assert.equal('string', data.response.distrStatusTopicName);
                assert.equal('OK', data.response.notificationUnregisterResult);
                assert.equal('OK', data.response.statusUnregisterResult);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DistributionServlet', 'unRegisterForDistribution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getValidArtifactTypes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getValidArtifactTypes((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DistributionServlet', 'getValidArtifactTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUebServerList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUebServerList((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.uebServerList));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DistributionServlet', 'getUebServerList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const artifactExternalServletUuid = 'fakedata';
    const artifactExternalServletOperationUUID = 'fakedata';
    const artifactExternalServletUploadInterfaceOperationArtifactBodyParam = {
      payloadData: [
        'string'
      ],
      listHeatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: false,
          ownerId: 'string'
        }
      ],
      creationDate: 6,
      timeout: 2,
      esId: 'string',
      description: 'string',
      artifactLabel: 'string',
      artifactUUID: 'string',
      artifactVersion: 'string',
      heatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: false,
          ownerId: 'string'
        }
      ],
      artifactGroupType: 'TOSCA',
      heatParamsUpdateDate: 2,
      artifactChecksum: 'string',
      generatedFromId: 'string',
      mandatory: true,
      serviceApi: true,
      payloadUpdateDate: 2,
      artifactName: 'string',
      artifactType: 'string',
      artifactRef: 'string',
      apiUrl: 'string',
      artifactRepository: 'string',
      userIdCreator: 'string',
      artifactCreator: 'string',
      userIdLastUpdater: 'string',
      updaterFullName: 'string',
      isFromCsar: false,
      requiredArtifacts: [
        'string'
      ],
      creatorFullName: 'string',
      artifactDisplayName: 'string',
      uniqueId: 'string',
      generated: true,
      duplicated: true,
      lastUpdateDate: 5,
      empty: true,
      ownerId: 'string'
    };

    describe('#uploadInterfaceOperationArtifact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadInterfaceOperationArtifact(artifactExternalServletUuid, artifactExternalServletOperationUUID, 'fakedata', artifactExternalServletUploadInterfaceOperationArtifactBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.payloadData));
                assert.equal(true, Array.isArray(data.response.listHeatParameters));
                assert.equal(9, data.response.creationDate);
                assert.equal(2, data.response.timeout);
                assert.equal('string', data.response.esId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.artifactLabel);
                assert.equal('string', data.response.artifactUUID);
                assert.equal('string', data.response.artifactVersion);
                assert.equal(true, Array.isArray(data.response.heatParameters));
                assert.equal('SERVICE_API', data.response.artifactGroupType);
                assert.equal(1, data.response.heatParamsUpdateDate);
                assert.equal('string', data.response.artifactChecksum);
                assert.equal('string', data.response.generatedFromId);
                assert.equal(true, data.response.mandatory);
                assert.equal(false, data.response.serviceApi);
                assert.equal(2, data.response.payloadUpdateDate);
                assert.equal('string', data.response.artifactName);
                assert.equal('string', data.response.artifactType);
                assert.equal('string', data.response.artifactRef);
                assert.equal('string', data.response.apiUrl);
                assert.equal('string', data.response.artifactRepository);
                assert.equal('string', data.response.userIdCreator);
                assert.equal('string', data.response.artifactCreator);
                assert.equal('string', data.response.userIdLastUpdater);
                assert.equal('string', data.response.updaterFullName);
                assert.equal(true, data.response.isFromCsar);
                assert.equal(true, Array.isArray(data.response.requiredArtifacts));
                assert.equal('string', data.response.creatorFullName);
                assert.equal('string', data.response.artifactDisplayName);
                assert.equal('string', data.response.uniqueId);
                assert.equal(false, data.response.generated);
                assert.equal(false, data.response.duplicated);
                assert.equal(2, data.response.lastUpdateDate);
                assert.equal(true, data.response.empty);
                assert.equal('string', data.response.ownerId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'uploadInterfaceOperationArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let artifactExternalServletArtifactUUID = 'fakedata';
    const artifactExternalServletAssetType = 'fakedata';
    const artifactExternalServletUploadArtifactBodyParam = {
      payloadData: [
        'string'
      ],
      listHeatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: true,
          ownerId: 'string'
        }
      ],
      creationDate: 2,
      timeout: 6,
      esId: 'string',
      description: 'string',
      artifactLabel: 'string',
      artifactUUID: 'string',
      artifactVersion: 'string',
      heatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: true,
          ownerId: 'string'
        }
      ],
      artifactGroupType: 'DEPLOYMENT',
      heatParamsUpdateDate: 3,
      artifactChecksum: 'string',
      generatedFromId: 'string',
      mandatory: true,
      serviceApi: true,
      payloadUpdateDate: 7,
      artifactName: 'string',
      artifactType: 'string',
      artifactRef: 'string',
      apiUrl: 'string',
      artifactRepository: 'string',
      userIdCreator: 'string',
      artifactCreator: 'string',
      userIdLastUpdater: 'string',
      updaterFullName: 'string',
      isFromCsar: false,
      requiredArtifacts: [
        'string'
      ],
      creatorFullName: 'string',
      artifactDisplayName: 'string',
      uniqueId: 'string',
      generated: false,
      duplicated: true,
      lastUpdateDate: 3,
      empty: true,
      ownerId: 'string'
    };

    describe('#uploadArtifact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadArtifact(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletUploadArtifactBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.payloadData));
                assert.equal(true, Array.isArray(data.response.listHeatParameters));
                assert.equal(6, data.response.creationDate);
                assert.equal(10, data.response.timeout);
                assert.equal('string', data.response.esId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.artifactLabel);
                assert.equal('string', data.response.artifactUUID);
                assert.equal('string', data.response.artifactVersion);
                assert.equal(true, Array.isArray(data.response.heatParameters));
                assert.equal('LIFE_CYCLE', data.response.artifactGroupType);
                assert.equal(8, data.response.heatParamsUpdateDate);
                assert.equal('string', data.response.artifactChecksum);
                assert.equal('string', data.response.generatedFromId);
                assert.equal(true, data.response.mandatory);
                assert.equal(true, data.response.serviceApi);
                assert.equal(4, data.response.payloadUpdateDate);
                assert.equal('string', data.response.artifactName);
                assert.equal('string', data.response.artifactType);
                assert.equal('string', data.response.artifactRef);
                assert.equal('string', data.response.apiUrl);
                assert.equal('string', data.response.artifactRepository);
                assert.equal('string', data.response.userIdCreator);
                assert.equal('string', data.response.artifactCreator);
                assert.equal('string', data.response.userIdLastUpdater);
                assert.equal('string', data.response.updaterFullName);
                assert.equal(true, data.response.isFromCsar);
                assert.equal(true, Array.isArray(data.response.requiredArtifacts));
                assert.equal('string', data.response.creatorFullName);
                assert.equal('string', data.response.artifactDisplayName);
                assert.equal('string', data.response.uniqueId);
                assert.equal(false, data.response.generated);
                assert.equal(true, data.response.duplicated);
                assert.equal(4, data.response.lastUpdateDate);
                assert.equal(true, data.response.empty);
                assert.equal('string', data.response.ownerId);
              } else {
                runCommonAsserts(data, error);
              }
              artifactExternalServletArtifactUUID = data.response.artifactUUID;
              saveMockData('ArtifactExternalServlet', 'uploadArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const artifactExternalServletUpdateArtifactBodyParam = {
      payloadData: [
        'string'
      ],
      listHeatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: false,
          ownerId: 'string'
        }
      ],
      creationDate: 8,
      timeout: 2,
      esId: 'string',
      description: 'string',
      artifactLabel: 'string',
      artifactUUID: 'string',
      artifactVersion: 'string',
      heatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: true,
          ownerId: 'string'
        }
      ],
      artifactGroupType: 'LIFE_CYCLE',
      heatParamsUpdateDate: 3,
      artifactChecksum: 'string',
      generatedFromId: 'string',
      mandatory: false,
      serviceApi: false,
      payloadUpdateDate: 5,
      artifactName: 'string',
      artifactType: 'string',
      artifactRef: 'string',
      apiUrl: 'string',
      artifactRepository: 'string',
      userIdCreator: 'string',
      artifactCreator: 'string',
      userIdLastUpdater: 'string',
      updaterFullName: 'string',
      isFromCsar: false,
      requiredArtifacts: [
        'string'
      ],
      creatorFullName: 'string',
      artifactDisplayName: 'string',
      uniqueId: 'string',
      generated: true,
      duplicated: true,
      lastUpdateDate: 4,
      empty: false,
      ownerId: 'string'
    };

    describe('#updateArtifact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateArtifact(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletArtifactUUID, artifactExternalServletUpdateArtifactBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.payloadData));
                assert.equal(true, Array.isArray(data.response.listHeatParameters));
                assert.equal(7, data.response.creationDate);
                assert.equal(6, data.response.timeout);
                assert.equal('string', data.response.esId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.artifactLabel);
                assert.equal('string', data.response.artifactUUID);
                assert.equal('string', data.response.artifactVersion);
                assert.equal(true, Array.isArray(data.response.heatParameters));
                assert.equal('OTHER', data.response.artifactGroupType);
                assert.equal(5, data.response.heatParamsUpdateDate);
                assert.equal('string', data.response.artifactChecksum);
                assert.equal('string', data.response.generatedFromId);
                assert.equal(true, data.response.mandatory);
                assert.equal(false, data.response.serviceApi);
                assert.equal(8, data.response.payloadUpdateDate);
                assert.equal('string', data.response.artifactName);
                assert.equal('string', data.response.artifactType);
                assert.equal('string', data.response.artifactRef);
                assert.equal('string', data.response.apiUrl);
                assert.equal('string', data.response.artifactRepository);
                assert.equal('string', data.response.userIdCreator);
                assert.equal('string', data.response.artifactCreator);
                assert.equal('string', data.response.userIdLastUpdater);
                assert.equal('string', data.response.updaterFullName);
                assert.equal(true, data.response.isFromCsar);
                assert.equal(true, Array.isArray(data.response.requiredArtifacts));
                assert.equal('string', data.response.creatorFullName);
                assert.equal('string', data.response.artifactDisplayName);
                assert.equal('string', data.response.uniqueId);
                assert.equal(true, data.response.generated);
                assert.equal(false, data.response.duplicated);
                assert.equal(3, data.response.lastUpdateDate);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.ownerId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'updateArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const artifactExternalServletResourceInstanceName = 'fakedata';
    const artifactExternalServletUploadArtifactToInstanceBodyParam = {
      payloadData: [
        'string'
      ],
      listHeatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: true,
          ownerId: 'string'
        }
      ],
      creationDate: 5,
      timeout: 1,
      esId: 'string',
      description: 'string',
      artifactLabel: 'string',
      artifactUUID: 'string',
      artifactVersion: 'string',
      heatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: true,
          ownerId: 'string'
        }
      ],
      artifactGroupType: 'OTHER',
      heatParamsUpdateDate: 7,
      artifactChecksum: 'string',
      generatedFromId: 'string',
      mandatory: true,
      serviceApi: true,
      payloadUpdateDate: 8,
      artifactName: 'string',
      artifactType: 'string',
      artifactRef: 'string',
      apiUrl: 'string',
      artifactRepository: 'string',
      userIdCreator: 'string',
      artifactCreator: 'string',
      userIdLastUpdater: 'string',
      updaterFullName: 'string',
      isFromCsar: false,
      requiredArtifacts: [
        'string'
      ],
      creatorFullName: 'string',
      artifactDisplayName: 'string',
      uniqueId: 'string',
      generated: false,
      duplicated: true,
      lastUpdateDate: 9,
      empty: false,
      ownerId: 'string'
    };

    describe('#uploadArtifactToInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.uploadArtifactToInstance(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletResourceInstanceName, artifactExternalServletUploadArtifactToInstanceBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.payloadData));
                assert.equal(true, Array.isArray(data.response.listHeatParameters));
                assert.equal(2, data.response.creationDate);
                assert.equal(9, data.response.timeout);
                assert.equal('string', data.response.esId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.artifactLabel);
                assert.equal('string', data.response.artifactUUID);
                assert.equal('string', data.response.artifactVersion);
                assert.equal(true, Array.isArray(data.response.heatParameters));
                assert.equal('DEPLOYMENT', data.response.artifactGroupType);
                assert.equal(1, data.response.heatParamsUpdateDate);
                assert.equal('string', data.response.artifactChecksum);
                assert.equal('string', data.response.generatedFromId);
                assert.equal(false, data.response.mandatory);
                assert.equal(false, data.response.serviceApi);
                assert.equal(5, data.response.payloadUpdateDate);
                assert.equal('string', data.response.artifactName);
                assert.equal('string', data.response.artifactType);
                assert.equal('string', data.response.artifactRef);
                assert.equal('string', data.response.apiUrl);
                assert.equal('string', data.response.artifactRepository);
                assert.equal('string', data.response.userIdCreator);
                assert.equal('string', data.response.artifactCreator);
                assert.equal('string', data.response.userIdLastUpdater);
                assert.equal('string', data.response.updaterFullName);
                assert.equal(true, data.response.isFromCsar);
                assert.equal(true, Array.isArray(data.response.requiredArtifacts));
                assert.equal('string', data.response.creatorFullName);
                assert.equal('string', data.response.artifactDisplayName);
                assert.equal('string', data.response.uniqueId);
                assert.equal(true, data.response.generated);
                assert.equal(false, data.response.duplicated);
                assert.equal(3, data.response.lastUpdateDate);
                assert.equal(true, data.response.empty);
                assert.equal('string', data.response.ownerId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'uploadArtifactToInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const artifactExternalServletUpdateArtifactOnResourceInstanceBodyParam = {
      payloadData: [
        'string'
      ],
      listHeatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: false,
          ownerId: 'string'
        }
      ],
      creationDate: 2,
      timeout: 3,
      esId: 'string',
      description: 'string',
      artifactLabel: 'string',
      artifactUUID: 'string',
      artifactVersion: 'string',
      heatParameters: [
        {
          uniqueId: 'string',
          name: 'string',
          type: 'string',
          description: 'string',
          currentValue: 'string',
          defaultValue: 'string',
          empty: true,
          ownerId: 'string'
        }
      ],
      artifactGroupType: 'DEPLOYMENT',
      heatParamsUpdateDate: 8,
      artifactChecksum: 'string',
      generatedFromId: 'string',
      mandatory: true,
      serviceApi: true,
      payloadUpdateDate: 8,
      artifactName: 'string',
      artifactType: 'string',
      artifactRef: 'string',
      apiUrl: 'string',
      artifactRepository: 'string',
      userIdCreator: 'string',
      artifactCreator: 'string',
      userIdLastUpdater: 'string',
      updaterFullName: 'string',
      isFromCsar: true,
      requiredArtifacts: [
        'string'
      ],
      creatorFullName: 'string',
      artifactDisplayName: 'string',
      uniqueId: 'string',
      generated: true,
      duplicated: false,
      lastUpdateDate: 1,
      empty: true,
      ownerId: 'string'
    };

    describe('#updateArtifactOnResourceInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateArtifactOnResourceInstance(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletArtifactUUID, artifactExternalServletResourceInstanceName, artifactExternalServletUpdateArtifactOnResourceInstanceBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.payloadData));
                assert.equal(true, Array.isArray(data.response.listHeatParameters));
                assert.equal(8, data.response.creationDate);
                assert.equal(5, data.response.timeout);
                assert.equal('string', data.response.esId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.artifactLabel);
                assert.equal('string', data.response.artifactUUID);
                assert.equal('string', data.response.artifactVersion);
                assert.equal(true, Array.isArray(data.response.heatParameters));
                assert.equal('DEPLOYMENT', data.response.artifactGroupType);
                assert.equal(3, data.response.heatParamsUpdateDate);
                assert.equal('string', data.response.artifactChecksum);
                assert.equal('string', data.response.generatedFromId);
                assert.equal(false, data.response.mandatory);
                assert.equal(true, data.response.serviceApi);
                assert.equal(7, data.response.payloadUpdateDate);
                assert.equal('string', data.response.artifactName);
                assert.equal('string', data.response.artifactType);
                assert.equal('string', data.response.artifactRef);
                assert.equal('string', data.response.apiUrl);
                assert.equal('string', data.response.artifactRepository);
                assert.equal('string', data.response.userIdCreator);
                assert.equal('string', data.response.artifactCreator);
                assert.equal('string', data.response.userIdLastUpdater);
                assert.equal('string', data.response.updaterFullName);
                assert.equal(true, data.response.isFromCsar);
                assert.equal(true, Array.isArray(data.response.requiredArtifacts));
                assert.equal('string', data.response.creatorFullName);
                assert.equal('string', data.response.artifactDisplayName);
                assert.equal('string', data.response.uniqueId);
                assert.equal(false, data.response.generated);
                assert.equal(true, data.response.duplicated);
                assert.equal(10, data.response.lastUpdateDate);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.ownerId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'updateArtifactOnResourceInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadComponentArtifact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadComponentArtifact(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletArtifactUUID, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'downloadComponentArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadResourceInstanceArtifact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadResourceInstanceArtifact(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletArtifactUUID, artifactExternalServletResourceInstanceName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'downloadResourceInstanceArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const assetMetadataExternalServletAssetType = 'fakedata';

    describe('#getAssetListExternal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssetListExternal(assetMetadataExternalServletAssetType, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AssetMetadataExternalServlet', 'getAssetListExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const assetMetadataExternalServletUuid = 'fakedata';

    describe('#getAssetSpecificMetadataByUuidExternal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssetSpecificMetadataByUuidExternal(assetMetadataExternalServletAssetType, assetMetadataExternalServletUuid, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AssetMetadataExternalServlet', 'getAssetSpecificMetadataByUuidExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getToscaModelExternal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getToscaModelExternal(assetMetadataExternalServletAssetType, assetMetadataExternalServletUuid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AssetMetadataExternalServlet', 'getToscaModelExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cRUDExternalServletAssetType = 'fakedata';
    const cRUDExternalServletCreateResourceExternalBodyParam = {};

    describe('#createResourceExternal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createResourceExternal(cRUDExternalServletAssetType, cRUDExternalServletCreateResourceExternalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CRUDExternalServlet', 'createResourceExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cRUDExternalServletLifecycleOperation = 'fakedata';
    const cRUDExternalServletChangeResourceStateExternalBodyParam = {
      userRemarks: 'string',
      action: 'UPGRADE_MIGRATION'
    };

    describe('#changeResourceStateExternal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.changeResourceStateExternal(cRUDExternalServletLifecycleOperation, 'fakedata', cRUDExternalServletAssetType, cRUDExternalServletChangeResourceStateExternalBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.invariantUUID);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.toscaModelURL);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CRUDExternalServlet', 'changeResourceStateExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const serviceActivationExternalServletServiceUUID = 'fakedata';
    const serviceActivationExternalServletOpEnvId = 'fakedata';
    const serviceActivationExternalServletActivateServiceExternalBodyParam = {};

    describe('#activateServiceExternal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.activateServiceExternal(serviceActivationExternalServletServiceUUID, serviceActivationExternalServletOpEnvId, serviceActivationExternalServletActivateServiceExternalBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-onap_sdc-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ServiceActivationExternalServlet', 'activateServiceExternal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteArtifact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteArtifact(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletArtifactUUID, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.payloadData));
                assert.equal(true, Array.isArray(data.response.listHeatParameters));
                assert.equal(6, data.response.creationDate);
                assert.equal(4, data.response.timeout);
                assert.equal('string', data.response.esId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.artifactLabel);
                assert.equal('string', data.response.artifactUUID);
                assert.equal('string', data.response.artifactVersion);
                assert.equal(true, Array.isArray(data.response.heatParameters));
                assert.equal('OTHER', data.response.artifactGroupType);
                assert.equal(4, data.response.heatParamsUpdateDate);
                assert.equal('string', data.response.artifactChecksum);
                assert.equal('string', data.response.generatedFromId);
                assert.equal(false, data.response.mandatory);
                assert.equal(true, data.response.serviceApi);
                assert.equal(8, data.response.payloadUpdateDate);
                assert.equal('string', data.response.artifactName);
                assert.equal('string', data.response.artifactType);
                assert.equal('string', data.response.artifactRef);
                assert.equal('string', data.response.apiUrl);
                assert.equal('string', data.response.artifactRepository);
                assert.equal('string', data.response.userIdCreator);
                assert.equal('string', data.response.artifactCreator);
                assert.equal('string', data.response.userIdLastUpdater);
                assert.equal('string', data.response.updaterFullName);
                assert.equal(false, data.response.isFromCsar);
                assert.equal(true, Array.isArray(data.response.requiredArtifacts));
                assert.equal('string', data.response.creatorFullName);
                assert.equal('string', data.response.artifactDisplayName);
                assert.equal('string', data.response.uniqueId);
                assert.equal(true, data.response.generated);
                assert.equal(true, data.response.duplicated);
                assert.equal(6, data.response.lastUpdateDate);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.ownerId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'deleteArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteArtifactOnResourceInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteArtifactOnResourceInstance(artifactExternalServletAssetType, artifactExternalServletUuid, artifactExternalServletArtifactUUID, artifactExternalServletResourceInstanceName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.payloadData));
                assert.equal(true, Array.isArray(data.response.listHeatParameters));
                assert.equal(9, data.response.creationDate);
                assert.equal(1, data.response.timeout);
                assert.equal('string', data.response.esId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.artifactLabel);
                assert.equal('string', data.response.artifactUUID);
                assert.equal('string', data.response.artifactVersion);
                assert.equal(true, Array.isArray(data.response.heatParameters));
                assert.equal('INFORMATIONAL', data.response.artifactGroupType);
                assert.equal(9, data.response.heatParamsUpdateDate);
                assert.equal('string', data.response.artifactChecksum);
                assert.equal('string', data.response.generatedFromId);
                assert.equal(false, data.response.mandatory);
                assert.equal(true, data.response.serviceApi);
                assert.equal(3, data.response.payloadUpdateDate);
                assert.equal('string', data.response.artifactName);
                assert.equal('string', data.response.artifactType);
                assert.equal('string', data.response.artifactRef);
                assert.equal('string', data.response.apiUrl);
                assert.equal('string', data.response.artifactRepository);
                assert.equal('string', data.response.userIdCreator);
                assert.equal('string', data.response.artifactCreator);
                assert.equal('string', data.response.userIdLastUpdater);
                assert.equal('string', data.response.updaterFullName);
                assert.equal(true, data.response.isFromCsar);
                assert.equal(true, Array.isArray(data.response.requiredArtifacts));
                assert.equal('string', data.response.creatorFullName);
                assert.equal('string', data.response.artifactDisplayName);
                assert.equal('string', data.response.uniqueId);
                assert.equal(true, data.response.generated);
                assert.equal(true, data.response.duplicated);
                assert.equal(7, data.response.lastUpdateDate);
                assert.equal(false, data.response.empty);
                assert.equal('string', data.response.ownerId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ArtifactExternalServlet', 'deleteArtifactOnResourceInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
