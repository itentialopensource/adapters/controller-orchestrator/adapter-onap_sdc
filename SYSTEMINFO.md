# ONAP Service Design and Creation

Vendor: ONAP
Homepage: https://www.onap.org/

Product: Service Design and Creation (SDC)
Product Page: https://docs.onap.org/projects/onap-sdc/en/latest/

## Introduction
We classify ONAP Service Design and Creation into the Data Center and Network Services domains as it helps to enable design, onboarding, and lifecycle management of network services and virtualized resources.

## Why Integrate
The ONAP Service Design Creation adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ONAP Service Design Creation. With this adapter you have the ability to perform operations such as:

- Service Activation

## Additional Product Documentation
[ONAP Service Design and Creation API Doc](https://docs.onap.org/projects/onap-sdc/en/latest/offeredapis.html)